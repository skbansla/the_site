from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from inventory import models
# Create your views here.

def index(request):
	return(render(request,'base/home.html',{}))

def profile(request):
	user = get_object_or_404(User,pk=request.user.id)
	# state,issues = get_object_or_404(models.issue_application,request_by__pk=request.user.id)
	issues = models.issue_application.objects.all().filter(request_by__id=request.user.id)
	context = {'user':user,'issues':issues}
	return(render(request,'base/profile.html',context))
