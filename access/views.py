from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import Group
from django.contrib import auth
from .forms import user_form, details_form
# Create your views here.

def login(request):
	if(request.user.is_authenticated()):
		return(HttpResponseRedirect('/'))
	elif(request.method=='POST'):
		username = request.POST['username']
		password = request.POST['password']
		user = auth.authenticate(username=username, password=password)
		if user and user.is_active:
			auth.login(request,user)
			return(HttpResponseRedirect('/'))
		else:
			return(render(request,'base/home.html',{'msg':'invalid login attempt'}))
	else:
		return(render(request,'base/home.html',{}))

def logout(request):
	if(request.user.is_authenticated()):
		auth.logout(request)
	else:
		return(render(request,'base/home.html',{'msg':'no user logged in'}))
	return(HttpResponseRedirect('/'))

def signup(request):
	if(request.method=="POST"):
		uf = user_form(request.POST)
		df = details_form(request.POST)
		if(uf.check_valid_pass(request)):
			return(render(request,'access/signup.html',{'msg':'Password Mismatch'}))
		elif(uf.is_valid() and df.is_valid()):
			user = uf.save(commit=False)
			passwd = uf.cleaned_data['password']
			group = Group.objects.get(name='student')
			user.set_password(passwd)
			user.save()
			user.groups.add(group)
			details = df.save(commit=False)
			details.user = user
			details.save()
			return(render(request,'base/home.html',{'msg':'account created'}))
		else:
			lst = [uf.errors,df.errors]
			return(render(request,'access/signup.html',{'msg':'following errors','error_details':lst}))
	else:
		return(render(request,'access/signup.html',{}))
