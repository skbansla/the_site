from django.db import models
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class member_details(models.Model):
	YEAR_CHOICE = (('1','first'),('2','Second'),('3','third'),('4','fourth'))
	BRANCH_CHOICE = (
		('AllBranches', 'None'),
		('CSE', 'Computer Science and Engineering'),
		('IT', 'Information Technology'),
		('EE', 'Electrical Engineering'),
		('ECE', 'Electronics and Communication Engineering'),
		('EEE', 'Electrical and Electronics Engineering'),
		('CE', 'Civil Engineering'),
		('IC', 'Instrumentation and Control Engineering'),
		('ME', 'Mechanical Engineering'),
		('MT', 'Manufacturing Technology'),
    )

	user = models.OneToOneField(User)
	year = models.CharField(max_length = 1, choices=YEAR_CHOICE, default='1')
	branch = models.CharField(max_length = 3, choices = BRANCH_CHOICE, default='ECE')

	def __str__(self):
		return self.user.username
