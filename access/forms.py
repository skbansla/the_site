from django import forms
from .models import member_details
from django.contrib.auth.models import User

class user_form(forms.ModelForm):
	def check_valid_pass(self,request):
		if(request.POST['password']==request.POST['pass2']):
			return False
		else:
			return True

	class Meta:
		model = User
		fields = ['username','password','email','first_name','last_name']

class details_form(forms.ModelForm):
	class Meta:
		model = member_details
		fields = ['branch','year']
		exclude = ['user']
