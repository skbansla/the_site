# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-07-31 19:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0004_auto_20170731_1807'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='storage',
            new_name='inventory',
        ),
        migrations.RenameField(
            model_name='accessories',
            old_name='equipment',
            new_name='successor_bot',
        ),
    ]
