from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class storage_locations(models.Model):
	location = models.CharField(max_length=30, default='eYantra Lab')

	def __str__(self):
		return(self.location)

class inventory(models.Model):
	bot_name = models.CharField(max_length=20)
	bot_count = models.IntegerField(default=0)
	bot_stored_place = models.ForeignKey(storage_locations, on_delete=models.CASCADE)

	def __str__(self):
		return self.bot_name

class accessories(models.Model):
	successor_bot = models.ForeignKey(inventory,on_delete=models.CASCADE)
	accessory = models.CharField(max_length=30)
	count = models.IntegerField(default=0)
	stored_place = models.ForeignKey(storage_locations,on_delete=models.CASCADE)

	def __str__(self):
		return self.accessory

class issue_application(models.Model):
	request_by = models.ForeignKey(User,on_delete=models.CASCADE)
	subject = models.CharField(max_length=50)
	application = models.CharField(max_length=350)
	passed = models.BooleanField(default=False)
	issued_on = models.DateTimeField('date_issue_requested')

	def __str__(self):
		return self.subject
