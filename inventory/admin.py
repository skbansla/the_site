from django.contrib import admin
from . import models

# Register your models here.

class bots(admin.TabularInline):
	model = models.inventory
	extra = 1

class bot_accessories(admin.TabularInline):
	model = models.accessories
	extra = 1

class inventory_admin(admin.ModelAdmin):
	fields = ['location']
	inlines = [bots,bot_accessories]

admin.site.register(models.storage_locations, inventory_admin)
admin.site.register(models.issue_application)
