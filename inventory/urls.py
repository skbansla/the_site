from django.conf.urls import url
from . import views

app_name = 'inventory'
urlpatterns = [
	url(r'^$',views.index,name="inventory_index"),
	url(r'^update/',views.update,name='update'),
	url(r'^update/.*',views.update,name='update'),
	url(r'^issue/',views.issue,name='issue'),
	url(r'^requests/',views.list_requests,name='requests'),
]
