from django import forms
from .models import storage_locations, inventory, accessories, issue_application

class loc_form(forms.ModelForm):
	class Meta:
		model = storage_locations
		fields = ['location']

class bot_form(forms.ModelForm):
	class Meta:
		model = inventory
		fields = ['bot_name','bot_count']
		exclude = ['bot_stored_place']

class accessory_form(forms.ModelForm):
	class Meta:
		model = accessories
		fields = ['accessory','count']
		exclude = ['successor_bot','stored_place']

class issue_details(forms.ModelForm):
	class Meta:
		model = issue_application
		fields = ['application','subject']
		exclude = ['request_by','issued_on']
