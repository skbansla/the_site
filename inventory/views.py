from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth.models import User
from itertools import chain
from django.utils import timezone
from . import models
from .forms import loc_form,bot_form, accessory_form, issue_details

# Create your views here.

def index(request):
	data = models.storage_locations.objects.all()
	data_for_page = {'data':data}
	return(render(request,'inventory/index.html',data_for_page))


def update(request): 		# location=None,bot=None,accessory=None):
	if(request.method=='POST'):
		loc_f = loc_form(request.POST)
		bot_f = bot_form(request.POST)
		acc_f = accessory_form(request.POST)
		if((loc_f.is_valid() and bot_f.is_valid()) and acc_f.is_valid()):
			location = models.storage_locations.objects.get(location__iexact=request.POST['location'])
			if(request.POST['choice']=='To update old inventory'):
				l = len(request.POST.getlist('bot_name'))
				for i in range(l):
					bot = models.inventory.objects.get(bot_name=request.POST.getlist('bot_name')[i])
					bot.bot_count = request.POST.getlist('bot_count')[i]
					bot.bot_stored_place = location
					bot.save()
				l = len(request.POST.getlist('accessory'))
				for i in range(l):
					acc = models.accessories.objects.get(accessory__iexact=request.POST.getlist('accessory')[i])
					acc.count = request.POST.getlist('count')[i]
					acc.stored_place = location
					acc.save()
				return(HttpResponseRedirect('/'))
			else:
				if(request.POST['bot_name']!='0'):
					bot = bot_f.save(commit=False)
					bot.bot_stored_place = location
					bot.save()
				if(request.POST['accessory']!='0'):
					accessory = acc_f.save(commit=False)
					accessory.stored_place = location
					bot = models.inventory.objects.get(bot_name__iexact=request.POST['successor_bot'])
					accessory.successor_bot = bot
					accessory.save()
				return(HttpResponseRedirect('/'))
		else:
			lst = [loc_f.errors,bot_f.errors,acc_f.errors]
			return(HttpResponse(lst))

	elif(request.GET.get('location')):
		location = models.storage_locations.objects.get(location__iexact=request.GET['location'])
		if(request.GET['choice']=='To update old inventory'):
			context = {'location':location,'selection0':True,'selection1':True,'choice':'To update old inventory'}
			return(render(request,'inventory/update_old.html',context))
		else:
			context = {'selection0':True,'selection1':True,'selection2':False,'location':location,'choice':'Add new items'}
			return(render(request,'inventory/update_new.html',context))
	elif(request.GET.get('choice')):
		data = models.storage_locations.objects.all()
		context = {'data':data,'selection0':True,'selection1':False}
		if(request.GET['choice']=='old'):
			context['choice'] = 'To update old inventory'
		else:
			context['choice'] = 'Add new items'
		return(render(request,'inventory/update.html',context))
	else:
		context = {'selection0':False,'selection1':False}
		return(render(request,'inventory/update.html',context))



def issue(request):
	if(request.method=='POST'):
		form = issue_details(request.POST)
		if(form.is_valid()):
			appl = form.save(commit=False)
			appl.request_by = request.user
			appl.issued_on = timezone.now()
			appl.save()
			return(HttpResponse("Request initiated"))
		else:
			return(HttpResponse(form.errors))
	else:
		bot_data = models.inventory.objects.all()
		accessory_data = models.accessories.objects.all()
		data = list(chain(bot_data,accessory_data))
		data_for_page = {'data':data}
		return(render(request,'inventory/issue.html',data_for_page))

def list_requests(request):
	if(request.method=='POST'):
		req = models.issue_application.objects.get(id=request.POST['id'])
		req.passed = True
		req.save()
		return(HttpResponseRedirect('inventory/requests'))
	else:
		data = models.issue_application.objects.all()
		data_for_page = {'data':data}
		return(render(request,'inventory/requests.html',data_for_page))

