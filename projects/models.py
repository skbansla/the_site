from django.db import models
# Create your models here.

class projects_list(models.Model):
	title = models.CharField(max_length=25)
	year = models.IntegerField(default=2016)
	team_name = models.CharField(max_length=20)
	details = models.CharField(max_length=150)
	published = models.DateTimeField('date_published')

	def publish_it(self):
		self.published = timezone.now()
		self.save()

	def __str__(self):
		return self.title
