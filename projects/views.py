from django.shortcuts import render
from django.http import HttpResponse
from .forms import project_add_form
from django.utils import timezone
from .models import projects_list

# Create your views here.

def index(request):
	projects = projects_list.objects.filter(published__lte = timezone.now()).order_by('-published')
	data_for_page = {'projects':projects}
	return(render(request,'projects/projects.html',data_for_page))

def add(request):
	if(request.method=='POST'):
		form = project_add_form(request.POST)
		if(form.is_valid()):
			details = form.save(commit=False)
			details.published = timezone.now()
			details.save()
			return(HttpResponse("ADDED SUCCESSFULLY"))
		else:
			return(HttpResponse(form.errors))
	else:
		return(render(request,'projects/add.html',{}))
