from django.conf.urls import url
from . import views

app_name = 'projects'
urlpatterns = [
	url(r'^$',views.index,name='projects_index'),
	url(r'^add/',views.add,name='add'),
]
