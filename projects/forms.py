from django import forms
from .models import projects_list

class project_add_form(forms.ModelForm):
	class Meta:
		model = projects_list
		fields = ['title','year','team_name','details']

